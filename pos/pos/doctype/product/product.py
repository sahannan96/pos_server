# -*- coding: utf-8 -*-
# Copyright (c) 2021, Hash and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Product(Document):
	pass

@frappe.whitelist()
def get_all_products():
    return frappe.db.get_list('Product', fields=["*"])
